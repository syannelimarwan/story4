from django.shortcuts import render, redirect
from .models import Schedule
from . import forms
from django.http import HttpResponse

def home(request):
	return render (request,'story3.html')

def about(request):
	context = {"about_page": "active"}
	return render (request, 'story3.1.html',context)

def contact(request):
	context = {"contact_page" : "active"}
	return render (request, 'story3.3.html', context)

def schedule(request):
    schedules = Schedule.objects.all().order_by("date")
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")
    
def schedule_hapus(request, id):
    if request.method == 'GET':
        schedule = Schedule.objects.get(id=id)
        schedule.delete()
        return redirect('schedule')

# Create your views here.
